import os
import sys
from threading import Thread, Lock, Event
import time
import socket as nativeSocket
import json
from mavlink_api_communication import FlightProcedures
# import RPi.GPIO as GPIO
from subprocess import Popen
import datetime

time_now = 0  # +1 every 0.5s
last_response = time_now
terminate_all = False
cycling = True
debug = 3
transmit = False

mavlink_device_string = "/dev/ttyAMA0"  # "tcp:192.168.1.235:5760" # "/dev/ttyAMA0"
#mavlink_device_string = "tcp:192.168.100.241:5760"
response_list = []
drone_commands = []
safe_cp_lock = Lock()
peripheral_service_ip = "192.168.3.1"
peripheral_service_port = 7001
local_service_port = 7002
e_start_now = Event()
drone_state = {'state': 'guided',
               'state_time': time_now,
               'system_state': 'standby',
               'alt': 0.0,
               'alt_time': time_now,
               'last_rc_input': time_now,
               'watch_over_rc_inputs': False,
               'home_position': None,
               'obey_rc_input': False
               }

system_state = {'high': {
                    'sensors': 0,
                    'threads': {
                        'watch_connection': 0,
                        'send_receive_drone_commands': 0,
                        'reply_all_data': 0,
                        'video_transmission': 0,
                        'watch_rc_inputs': 0
                    }}
    # ,
    #             'low': {
    #                 'pixhawk': 0
    #                 }
                }

drone_variables = {
    'camPort': '5002',
    'camIP': '192.168.100.241'
}


def lg(log_message, level):
    global response_list

    if level <= debug:
        print (datetime.datetime.now()),
        if level == 1:
            print("[CRITICAL ]"),
            response_list.append({'error': {'rpi_pixhawk': log_message}})
        if level == 2:
            print("[IMPORTANT]"),
        if level == 3:
            print("[VERBOSE  ]"),
        if isinstance(log_message, list):
            print(">>"),
            for message in log_message:
                print("<>"),
                print(message),
            print("<<")
        else:
            try:
                print(log_message)
            except:
                print('none asci message')


def getserial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        cpuserial = "ERROR000000000"

    return cpuserial


def getDroneVariables():
    global drone_variables
    cpu_serial = getserial()
    if cpu_serial == "000000009840105c":  #main (octo) drone unique ID
        drone_variables['camPort'] = '5002'
    elif cpu_serial == "00000000db8f2bd1":  # test (hex) drone unique ID
        drone_variables['camPort'] = '5001'


def start_video_transmission():
    global transmit
    this_t = 'start_video_transmission'
    try:
        os.chdir(sys.argv[1])  # go to video scripts directory
    except:
        lg([this_t, "Please pass video scripts directory_name", sys.argv[1]], 1)

    while not terminate_all:
        system_state['high']['threads']['video_transmission'] = time_now
        if transmit:
            lg([this_t,"starting transmission"] ,3)
            a_pid = Popen(["./a.out", drone_variables['camIP'], drone_variables['camPort']])
            time.sleep(0.5)  # wait for connection
            a_pid_code = a_pid.poll()  # get process status
            if a_pid_code is None:  # checking if connection is made and no error code received
                # rpivid = Popen(["/usr/bin/raspivid", "-t", "0", "-w", "1296", "-h", "972",
                #                 "-fps", "49", "-b", "5000000", "-o", "bufferin"])
                rpivid = Popen(["/usr/bin/raspivid", "-t", "0", "-w", "960", "-h", "960",
                                "-fps", "42", "-b", "20000000", "-sh", "50", "-o", "bufferin"])
                a_pid.wait()
                rpivid.terminate()

            else:
                lg([this_t,"start video transmission terminated. a_pid_code :", a_pid_code], 1)

            transmit = False
        time.sleep(1)


def system_error_state_handler():  #checks system state. if any wrong - rtl or land
    global drone_state
    global drone_commands
    global response_list
    this_t = 'system_error_state_handler'

    while not terminate_all:
        lg([this_t,"system_error_state_handler loop"], 3)
        system_state_severity = 0  # 0 - all good, 1 - rtl via dedicated thread, 2 - send rtl directly to controller
        current_time = time_now

        if current_time - system_state['high']['threads']['video_transmission'] > 4 and not transmit:
            lg([this_t,"THREAD start_video_transmission is dead !!!"], 1)

        # variable system_state['high']['sensors'] is being updated from watch_connection THREAD
        # if system_state['high']['sensors'] == 0:  # no data from sensors server. initiated 'rtl' command
        #     system_state_severity = 1
        #     lg(['system_error_state_handler',"no sensors data detected !!!"], 1)

        # checks any communication between pix and sensors (main cycle)
        if current_time - system_state['high']['threads']['watch_connection'] > 4:
            system_state_severity = 1
            lg([this_t,"THREAD watch_connection is dead !!!"], 1)

        # replys all data to sensors
        if current_time - system_state['high']['threads']['reply_all_data'] > 4:
            lg([this_t,"THREAD reply_all_data is dead !!!"], 1)
            system_state_severity = 1

        # we havo no rc input thread. set rtl
        if current_time - system_state['high']['threads']['watch_rc_inputs'] > 6:
            lg([this_t,"THREAD watch_rc_inputs is dead !!!"], 1)
            system_state_severity = 1

        # Thread send_receive_drone_commands sends commands to flight controller
        if current_time - system_state['high']['threads']['send_receive_drone_commands'] > 4:
            system_state_severity = 2
            lg([this_t,"THREAD send_receive_drone_commands is dead !!!"], 1)

        if system_state_severity == 2 and drone_state['system_state'] == 'active':
            response_list.append(set_drone_mode_state('rtl'))
            lg([this_t, "system_state_severity", system_state_severity, 'doing direct RTL'], 1)
            time.sleep(30)  # additionl execute command every 30s

        elif system_state_severity == 1 and drone_state['system_state'] == 'active':
            drone_commands.append('rtl')
            lg([this_t, "system_state_severity", system_state_severity, 'appending cmd RTL'], 1)
            time.sleep(30)  # additionl execute command every 30s
        else:
            time.sleep(2)
            lg([this_t, "all processes good", 'sleep 2s'], 3)

    time.sleep(1)


#
# watch rc input request, if there is none for 2s, set to defaults,
# if no input for 2min - rtl and switch off watching.
# new rc_1234 command will turn on watch_over_rc_inputs, and monitoring will start again
#

def watch_rc_inputs():
    this_t = 'watch_rc_inputs'
    global drone_commands
    global drone_state

    while not terminate_all:

        wait_for_data = False

        # check if watching input state is enabled
        if drone_state['watch_over_rc_inputs']:

            if (time_now - drone_state['last_rc_input']) > 4:  # if more than 2s without rc input

                if (time_now - drone_state['alt_time']) > 4:  # if altitude is registered within 2s
                    drone_commands.append('get_position')
                    wait_for_data = True

                if (time_now - drone_state['state_time']) > 6:  # if last state is registered within 3s
                    drone_commands.append('get_state')
                    wait_for_data = True

                if wait_for_data:  # we asked pix state or position so wait for response
                    time.sleep(1)
                    system_state['high']['threads']['watch_rc_inputs'] = time_now  #update time stamp just in case

                # if drone is in the air and in LOITER/POSHOLD manual controll mode
                if drone_state['alt'] > 2.0 and drone_state['state'] in ['loiter', 'poshold']:
                    #if drone no input did not reach 2m delay
                    if (time_now - drone_state['last_rc_input']) < 240:  # if less than 2min

                        # append loiter or poshold (rc channels are automatically set to default 'rc_1234')
                        drone_commands.append(drone_state['state'])
                        lg([this_t, 'no rc_input for > 4s, Going ' + drone_state['state']], 1)

                    else:
                        #somethings wrong with rc input, set go home location and turn off rc input watching
                        drone_commands.append('rtl')
                        drone_state['watch_over_rc_inputs'] = False
                        lg([this_t, 'FAILSAFE[watch_rc_inputs]: no rc_input for > 2min, Going RTL'], 1)

                    time.sleep(1)
        time.sleep(1)
        system_state['high']['threads']['watch_rc_inputs'] = time_now


def set_drone_mode_state(set_state):
    global  drone_state
    return_val = None
    if set_state == 'guided':
        return_val = flight_procedures.guided()
        drone_state['state'] = 'guided'
        drone_state['state_time'] = time_now

    elif set_state == 'rtl':
        return_val = flight_procedures.go_home()
        drone_state['state'] = 'rtl'
        drone_state['state_time'] = time_now

    elif set_state == 'land':
        return_val = flight_procedures.land()
        drone_state['state'] = 'land'
        drone_state['state_time'] = time_now

    elif set_state == 'poshold':
        return_val = flight_procedures.poshold()
        drone_state['state'] = 'poshold'
        drone_state['state_time'] = time_now

    elif set_state == 'loiter':
        return_val = flight_procedures.loiter()
        drone_state['state'] = 'loiter'
        drone_state['state_time'] = time_now

    elif set_state == 'stab':
        return_val = flight_procedures.stabilize()
        drone_state['state'] = 'stab'
        drone_state['state_time'] = time_now

    return return_val

#
# check connection between this service and sensor service
#


def watch_connection():
    this_t = 'watch_connection'
    global terminate_all
    global flight_procedures
    global drone_commands
    while True:
        lg([this_t, 'watch_connection loop'], 3)
        system_state['high']['threads']['watch_connection'] = time_now
        if terminate_all:  # go home and land
            if drone_state['alt'] > 2.0:
                time.sleep(2)
                set_drone_mode_state('rtl')
                time.sleep(1)
            flight_procedures.close_connection()
            return

        delta_time = time_now - last_response
        if delta_time > 4:
            system_state['high']['sensors'] = 0
            if delta_time > 120:
                lg([this_t, "FAILSAFE connection lost for more than 1m, going to RTL mode !!!"], 1)
                drone_commands.append('rtl')
                time.sleep(3)
            else:
                lg([this_t, "connection lost for more than 2s !!!"], 1)
        else:
            system_state['high']['sensors'] = 1
        time.sleep(1)


def low_precision_clock():
    global time_now
    while not terminate_all:
        time.sleep(0.5)
        time_now = time_now + 1


def send_receive_drone_commands(e_start_now):
    this_t = 'send_receive_drone_commands'
    global terminate_all
    global drone_commands
    global response_list
    global drone_state
    clean_channels = {'1': None, '2': None, '3': None, '4': None, }  #roll, pitch, throttle, yaw

    while True and not terminate_all:
        lg([this_t, "send_receive_drone_commands loop"], 3)
        if drone_commands:
            lg([this_t, 'Full drone cmd', drone_commands], 3)
            drone_cmds_no_dup_tmp = drone_commands
            drone_commands = []
            full_command = len(drone_cmds_no_dup_tmp)
            #remove duplicates
            drone_cmds_no_dup_tmp = list(dict.fromkeys(drone_cmds_no_dup_tmp))

            #remove duplicate rc sets
            rc_1234 = None
            drone_cmds_no_dup = []
            for cmd_tmp in drone_cmds_no_dup_tmp:
                if cmd_tmp.startswith('rc_1234'):
                    rc_1234 = cmd_tmp
                else:
                    drone_cmds_no_dup.append(cmd_tmp)

            if rc_1234 is not None:
                drone_cmds_no_dup.append(rc_1234)

            if len(drone_cmds_no_dup) != full_command:
                lg([this_t, 'No dupl drone cmd', drone_cmds_no_dup],3)

        else:
            drone_cmds_no_dup = []

        while drone_cmds_no_dup:  # if drone command exist - execute
            lg([this_t, "send_receive_drone_commands executing commands"], 3)
            drone_exec = drone_cmds_no_dup[0]
            lg([this_t, drone_cmds_no_dup], 3)

            drone_cmds_no_dup.remove(drone_exec)
            if drone_exec == 'arm':
                drone_state['home_position'] = flight_procedures.get_home_position()
                if drone_state['home_position'] is not None:
                    # always arm in guided mode
                    response_list.append(set_drone_mode_state('guided'))
                    response_list.append(flight_procedures.arm())

                    # home position chages after arming, so get new
                    lg([this_t, 'updating home_position', drone_state['home_position']], 3)
                    drone_state['home_position'] = flight_procedures.get_home_position()
                    response_list.append({'home_position': drone_state['home_position']})
                else:
                    lg([this_t, 'cant arm, home position is NONE'], 1)

            if drone_exec == 'disarm':
                response_list.append(flight_procedures.disarm())

            if drone_exec == 'man':
                response_list.append(set_drone_mode_state('guided'))
                tmp_channels = flight_procedures.set_rc_1234("rc_1234",clean_channels)
                drone_state['obey_rc_input'] = True

                # do failsafe chack on throttle (and other manual rc input)
                if 1300 < tmp_channels['rc_channels']['throttle'] < 1700 and \
                        1400 < tmp_channels['rc_channels']['roll'] < 1600 and \
                        1400 < tmp_channels['rc_channels']['pitch'] < 1600 and \
                        1400 < tmp_channels['rc_channels']['yaw'] < 1600:
                    # if sticks are in the middle
                    response_list.append(tmp_channels)
                else:
                    lg([this_t, 'Sticks are not in the middle. Switching to digital mode. sticks:',tmp_channels], 1)
                    response_list.append(flight_procedures.set_rc_1234("rc_1234"))
                    drone_state['obey_rc_input'] = False
                response_list.append({'obey_rc_input': drone_state['obey_rc_input']})

            if drone_exec == 'dig':
                response_list.append(set_drone_mode_state('guided'))
                drone_state['obey_rc_input'] = False
                response_list.append(flight_procedures.set_rc_1234("rc_1234"))
                response_list.append({'obey_rc_input': drone_state['obey_rc_input']})

            if drone_exec == 'go_home_position':  # returns to home position but never lands
                if drone_state['home_position'] is not None:
                    response_list.append(flight_procedures.go_home_position(drone_state['home_position'], 5))
                    drone_state['state'] = 'guided'
                    drone_state['state_time'] = time_now
                    response_list.append({"vehicle_mode": "guided"})

            if drone_exec == 'get_home_position':
                if drone_state['home_position'] is not None:
                    response_list.append({'home_position': drone_state['home_position']})
                else:
                    drone_state['home_position'] = flight_procedures.get_home_position()
                    response_list.append({'home_position': drone_state['home_position']})

            if drone_exec == 'take_off':
                take_off_altitude = 5
                tmp_msg = flight_procedures.take_off(take_off_altitude)  # take off to 5m from ground
                response_list.append(tmp_msg)
                if "doing_state" in tmp_msg:  # if got state and no errors
                    if 'taking_off' == tmp_msg['doing_state']:  # if taking of
                        for x in range(1, (take_off_altitude + 1)):  # respond about altitude every "take_off_altitude" second
                            response_list.append(flight_procedures.get_altitude())
                            system_state['high']['threads']['send_receive_drone_commands'] = time_now
                            time.sleep(1)

            if drone_exec == 'land':
                response_list.append(set_drone_mode_state('land'))

            if drone_exec == 'rtl':  # returns to landing site
                response_list.append(set_drone_mode_state('rtl'))

            if drone_exec == 'guided':
                response_list.append(set_drone_mode_state('guided'))

            if drone_exec == 'stab':
                response_list.append(set_drone_mode_state('stab'))

            if drone_exec == 'loiter':
                response_list.append(set_drone_mode_state('loiter'))
                response_list.append(flight_procedures.set_rc_1234("rc_1234"))

            if drone_exec == 'poshold':
                response_list.append(set_drone_mode_state('poshold'))
                response_list.append(flight_procedures.set_rc_1234("rc_1234"))

            if drone_exec == 'get_state':
                tmp_data_storage = flight_procedures.get_status()
                drone_state['state'] = (tmp_data_storage['vehicle_mode']).lower()
                drone_state['system_state'] = (tmp_data_storage['system_state']).lower()
                drone_state['state_time'] = time_now
                response_list.append(tmp_data_storage)

            if drone_exec == 'get_altitude':
                tmp_data_storage = flight_procedures.get_altitude()
                lg([this_t, "get_altitude not registered!!!  do fix for safety!!!"], 1)
                lg([this_t, tmp_data_storage], 1)
                response_list.append(tmp_data_storage)

            if drone_exec == 'get_position':
                tmp_data_storage = flight_procedures.get_position()
                drone_state['alt'] = tmp_data_storage['gps']['alt']
                drone_state['alt_time'] = time_now
                response_list.append(tmp_data_storage)

            if drone_exec == 'get_rc_ch':
                response_list.append(flight_procedures.get_rc_ch())
                response_list.append({'obey_rc_input':drone_state['obey_rc_input']})

            if drone_exec.startswith('rc_1234'):
                drone_state['last_rc_input'] = time_now  # register rc imput time stamp
                drone_state['watch_over_rc_inputs'] = True

                if not drone_state['obey_rc_input']:  # obey radio rc input over digital
                    response_list.append(flight_procedures.set_rc_1234(drone_exec))
                else:
                    response_list.append(flight_procedures.set_rc_1234(drone_exec, clean_channels))

            time.sleep(0.02)

        system_state['high']['threads']['send_receive_drone_commands'] = time_now

        e_start_now.wait(0.5)  # sleep for .5s or until we get event command
        e_start_now.clear()  # clearing event for new listenning

#
# replay all data existing in list
#


def reply_all_data():
    this_t = 'reply_all_data'
    global response_list

    # set up data socket to be send to peripheral service
    sock_send = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)

    while True and not terminate_all:
        lg([this_t, "reply_all_data loop"], 3)
        lg([this_t, response_list], 3)
        while response_list:
            lg([this_t, "reply_all_data sending command"], 3)
            response_msg = response_list[0]
            safe_cp_lock.acquire()  # lock variable for modification
            response_list.remove(response_msg)
            safe_cp_lock.release()  # release variable lock

            sock_send.sendto(json.dumps(response_msg), (peripheral_service_ip, peripheral_service_port))

        system_state['high']['threads']['reply_all_data'] = time_now
        time.sleep(0.5)  # responce time is not important so sleep .5s and respond what we have gathered

    sock_send.close()

getDroneVariables()

# set up data socket to get peripheral request
sock_get = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)
sock_get.bind(("", local_service_port))

flight_procedures = FlightProcedures(mavlink_device_string)

# start low precision clock
try:
    t_low_precision_clock = Thread(target=low_precision_clock)
    t_low_precision_clock.start()
except:
    lg("Error: unable to start low_precision_clock thred", 1)

# start thread to watch if connection between drone and station is ok
try:
    t_ping_pong = Thread(target=watch_connection)
    t_ping_pong.start()
except:
    lg("Error: unable to start connection watch thread", 1)

# start thread for talking to pixhawk controller
try:
    t_drone_cmds = Thread(target=send_receive_drone_commands, args=(e_start_now,))
    t_drone_cmds.start()
except:
    lg("Error: unable to start send_receive_drone_commands thread", 1)

try:
    t_respond_cmds = Thread(target=reply_all_data)
    t_respond_cmds.start()
except:
    lg("Error: unable to start data reply thread", 1)

# start thread to sen data to indicator led
try:
    t_system_error_state_handler= Thread(target=system_error_state_handler)
    t_system_error_state_handler.start()
except:
    lg("Error: unable to start system_error_state_handler thread", 1)

try:
    t_start_video_transmission = Thread(target=start_video_transmission)
    t_start_video_transmission.start()
except:
    lg("Error: unable to start start_video_transmission", 1)

try:
    t_watch_rc_inputs = Thread(target=watch_rc_inputs)
    t_watch_rc_inputs.start()
except:
    lg("Error: unable to start watch_rc_inputs", 1)


# start main cycle
# get commands and sanityze them, and fill drone_commands bucket queue
this_t = 'MAIN_cycling'
while cycling:
    message = {'msg': ''}

    lg([this_t, "cycling"], 3)

    got_invalid_msg = True
    server_message, server_ip = sock_get.recvfrom(1024)

    lg([this_t,"Received request: ", server_message], 3)

    message = json.loads(server_message)
    last_response = time_now

    if message['msg'] in ['velocity', 'yaw', 'arm', 'disarm', 'take_off', 'guided', 'get_altitude', 'get_position',
                          'get_home_position', 'land', 'stab', 'loiter', 'poshold', 'rtl', 'get_rc_ch', 'set_rc_ch',
                          'get_state', 'go_home_position', 'man', 'dig']:
        lg([this_t, "got "+message['msg']+" request"], 3)
        got_invalid_msg = False
        drone_commands.append(message['msg'])

    if message['msg'] == 'vid':
        lg([this_t,"got video request"], 3)
        got_invalid_msg = False
        transmit = True

    if (message['msg']).startswith('rc_1234'):
        got_invalid_msg = False
        drone_commands.append(message['msg'])

    if message['msg'] == '9':  # do failsafe check (drone state) if we can shut down the client
        got_invalid_msg = False
        lg([this_t, "quiting pixhawk client"], 1)
        drone_commands.append('rtl')
        response_list.append({'client_state': 'shutting_down'})
        e_start_now.set()  # inform about execution
        time.sleep(0.1)
        cycling = False

    if message['msg'] == 'ping':
        got_invalid_msg = False
        response_list.append({'heartbeat': 'pong'})

    if got_invalid_msg:
        response_list.append({'msg': "not recognized"})
        lg([this_t, 'got invalid message', message['msg']], 1)

    e_start_now.set()  # inform pixhawk controller that we have data to be proccessed

time.sleep(1)
sock_get.close()
terminate_all = True
time.sleep(3)
