#!/bin/bash

while true; do
    today=`date '+%Y_%m_%d__%H_%M_%S'`;
    script_path="/home/pi/rpi_pixhawk"
    /usr/bin/python ${script_path}/pixhawk_client.py ${script_path}/x264_stuff > /tmp/pixhawk_client_log$today 2>&1 ;
    sleep 2;
done