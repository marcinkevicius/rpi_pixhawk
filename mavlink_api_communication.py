from dronekit import connect, VehicleMode, LocationGlobalRelative
# from pymavlink import mavutil
import time


class FlightProcedures:
    """Complete flight procedures for full and autonomus flight"""

    def __init__(self, connection_string):
        self.connection_string = connection_string
        print 'Connecting to vehicle on: %s' % connection_string
        self.vehicle = connect(connection_string, baud=57600, wait_ready=True)

    def close_connection(self):
        self.vehicle.close()
        return True

    def test_check(self):
        self.arm()
        # Confirm vehicle armed
        time.sleep(1)
        self.disarm()

    def get_status(self):

        arm_status = 'disarmed'
        system_state = self.vehicle.system_status.state.lower()
        if system_state == 'active':  # its in the air so armed
            arm_status = 'armed'
        elif self.vehicle.armed:  # its not in the air, so check if armed
            arm_status = 'armed'

        print("System status: %s" % system_state)
        vehicle_mode = (self.vehicle.mode.name.lower())
        if vehicle_mode == 'stabilize':
            vehicle_mode = 'stab'

        return {"system_state": system_state,
                "arm_msg": arm_status,
                'vehicle_mode': vehicle_mode,
                "gps_fix": self.vehicle.gps_0.fix_type,
                "gps_num_sat": self.vehicle.gps_0.satellites_visible}

    def arm(self):
        if self.vehicle.armed:
            return {"arm_msg": 'armed'}
        print "Basic pre-arm checks"
        # Don't try to arm until autopilot is ready

        i = 0
        while not self.vehicle.is_armable:
            i += 1
            print " Waiting for vehicle to initialise..."
            time.sleep(1)
            if i > 10:
                self.get_status()
                return {"arm_msg": "error", "info": "not armable, check status"}

        print "Arming motors"
        # Copter should arm in GUIDED mode
        self.vehicle.mode = VehicleMode("GUIDED")
        self.vehicle.armed = True

        # Confirm vehicle armed

        i = 0
        while not self.vehicle.armed:
            i += 1
            print " Waiting for arming..."
            time.sleep(1)
            if i > 10:
                return {"arm_msg": "error", "info": "could not arm"}

        return {"arm_msg": "armed"}

    def disarm(self):
        i = 0
        if not self.vehicle.armed:
            print "vehicle already disarmed!!!"
            return {"arm_msg": "error", "info": "already disarmed"}
        self.vehicle.armed = False
        while self.vehicle.armed:
            i += 1
            print " Waiting for disarming..."
            time.sleep(1)
            if i > 10:
                return {"arm_msg": "error", "info": "could not disarm"}

        return {"arm_msg": 'disarmed'}

    def take_off(self, alt):
        if not self.vehicle.armed:
            return {"arm_msg": "error", "info": "not armed"}

        if self.vehicle.gps_0.fix_type and self.vehicle.gps_0.satellites_visible > 6:
            self.vehicle.simple_takeoff(alt)
            return {"doing_state": "taking_off"}

        return {"gps_fix": "no gps fix"}

    def land(self):
        self.vehicle.mode = VehicleMode("LAND")
        return {"doing_state": "landing"}

    def stabilize(self):
        self.vehicle.mode = VehicleMode("STABILIZE")
        return {"vehicle_mode": "stab"}

    def go_home(self):
        self.vehicle.mode = VehicleMode("RTL")
        return {"vehicle_mode": "rtl"}

    def guided(self):
        self.vehicle.mode = VehicleMode("GUIDED")
        return {"vehicle_mode": "guided"}

    def loiter(self):
        self.vehicle.mode = VehicleMode("LOITER")
        return {"vehicle_mode": "loiter"}

    def poshold(self):
        self.vehicle.mode = VehicleMode("POSHOLD")
        return {"vehicle_mode": "poshold"}

    def get_altitude(self):
        return {"altitude": self.vehicle.location.global_relative_frame.alt}

    def get_position(self):
        return {'gps':
                    {'long': self.vehicle.location.global_frame.lon,
                     'lat': self.vehicle.location.global_frame.lat,
                     'alt': self.vehicle.location.global_relative_frame.alt
                     }
                }

    def get_home_position(self):
        cmds = self.vehicle.commands
        cmds.download()
        cmds.wait_ready()
        home = self.vehicle.home_location
        if home is None:
            return None
        else:
            return {"long": home.lon, 'lat': home.lat}

    def go_alt_position(self, alt):
        self.vehicle.mode = VehicleMode("GUIDED")
        # print 'go_alt_position GUIDED'
        a_location = LocationGlobalRelative(self.vehicle.location.global_frame.lat, self.vehicle.location.global_frame.lon, alt)
        # print 'go_alt_position set location'
        self.vehicle.simple_goto(a_location)
        # print 'go_alt_position send to controller'
        # time.sleep(1)
        return {'doing_state','going to altitude:'+str(alt)}

    def go_home_position(self, home_position, alt):
        #self.vehicle.mode = VehicleMode("GUIDED")
        self.go_alt_position(alt)
        a_location = LocationGlobalRelative(home_position['lat'], home_position['long'], alt)
        self.vehicle.simple_goto(a_location)
        return {"doing_state": "going to home position"}


    # def send_ned_velocity(self, velocity_x, velocity_y, velocity_z):
    #     """
    #     Move vehicle in direction based on specified velocity vectors.
    #     """
    #     msg = self.vehicle.message_factory.set_position_target_local_ned_encode(
    #         0,  # time_boot_ms (not used)
    #         0, 0,  # target system, target component
    #         mavutil.mavlink.MAV_FRAME_LOCAL_NED,  # frame
    #         0b0000111111000111,  # type_mask (only speeds enabled)
    #         0, 0, 0,  # x, y, z positions (not used)
    #         velocity_x, velocity_y, velocity_z,  # x, y, z velocity in m/s
    #         0, 0, 0,  # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
    #         0, 0)  # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
    #
    #     # send command to vehicle
    #     self.vehicle.send_mavlink(msg)

    # def condition_yaw(self, heading):
    #     """
    #     Send MAV_CMD_CONDITION_YAW message to point vehicle at a specified heading (in degrees).
    #     This method sets an absolute heading by default, but you can set the `relative` parameter
    #     to `True` to set yaw relative to the current yaw heading.
    #     By default the yaw of the vehicle will follow the direction of travel. After setting
    #     the yaw using this function there is no way to return to the default yaw "follow direction
    #     of travel" behaviour (https://github.com/diydrones/ardupilot/issues/2427)
    #     For more information see:
    #     http://copter.ardupilot.com/wiki/common-mavlink-mission-command-messages-mav_cmd/#mav_cmd_condition_yaw
    #     """
    #     relative = True
    #     if relative:
    #         is_relative = 1  # yaw relative to direction of travel
    #     else:
    #         is_relative = 0  # yaw is an absolute angle
    #     # create the CONDITION_YAW command using command_long_encode()
    #     msg = self.vehicle.message_factory.command_long_encode(
    #         0, 0,  # target system, target component
    #         mavutil.mavlink.MAV_CMD_CONDITION_YAW,  # command
    #         0,  # confirmation
    #         heading,  # param 1, yaw in degrees
    #         0,  # param 2, yaw speed deg/s
    #         1,  # param 3, direction -1 ccw, 1 cw
    #         is_relative,  # param 4, relative offset 1, absolute angle 0
    #         0, 0, 0)  # param 5 ~ 7 not used
    #     # send command to vehicle
    #     self.vehicle.send_mavlink(msg)

    def get_rc_ch(self):
        print "\nChannel overrides: %s" % self.vehicle.channels.overrides
        return {"rc_channels": {"roll": self.vehicle.channels['1'], "pitch": self.vehicle.channels['2'],
                                "throttle": self.vehicle.channels['3'], "yaw": self.vehicle.channels['4'],
                                "5th": self.vehicle.channels['5']}}

    def set_rc_1234(self, encoded_channels, decoded_channels={}):
        channels_prepared = {'1': 1500, '2': 1500, '3': 1500, '4': 1500}
        rc_channels = encoded_channels.split(' ')
        rc_channels.remove('rc_1234')
        for rc_channel in rc_channels:
            if rc_channel.startswith('r'):  # if roll -> 1
                channels_prepared['1'] = int(rc_channel[1:])
                if channels_prepared['1'] < 1000 or channels_prepared['1'] > 2000:
                    channels_prepared['1'] = 1500

            if rc_channel.startswith('p'):  # if pitch -> 2
                channels_prepared['2'] = int(rc_channel[1:])
                if channels_prepared['2'] < 1000 or channels_prepared['2'] > 2000:
                    channels_prepared['2'] = 1500

            if rc_channel.startswith('t'):  # if throttle -> 3
                channels_prepared['3'] = int(rc_channel[1:])
                if channels_prepared['3'] < 1000 or channels_prepared['3'] > 2000:
                    channels_prepared['3'] = 1500

            if rc_channel.startswith('y'):  # if yaw -> 1
                channels_prepared['4'] = int(rc_channel[1:])
                if channels_prepared['4'] < 1000 or channels_prepared['4'] > 2000:
                    channels_prepared['4'] = 1500

        for decoded_channel_nr in decoded_channels:  # overade digital with analog data
            channels_prepared[decoded_channel_nr] = decoded_channels[decoded_channel_nr]

        self.vehicle.channels.overrides = channels_prepared
        time.sleep(0.3)
        return self.get_rc_ch()
