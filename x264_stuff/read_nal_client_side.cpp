#include <stdio.h>
#include <sys/time.h>
#include <wiringPi.h>
#include <cstring>
#include <memory.h>

static struct timeval t1, t2, ttmp;

PI_THREAD (lowPwm) {
	for (;;)
  	{
    		digitalWrite (0, HIGH) ; delay (200) ;
    		digitalWrite (0,  LOW) ; delay (800) ;
  	}
}

void syncClock (void) {

	short partial_result;
	gettimeofday(&ttmp, NULL);
	partial_result = (ttmp.tv_sec - t1.tv_sec) * 1000 + (ttmp.tv_usec - t1.tv_usec)/1000;

//	printf(" %lu llllllllllllll\n ", partial_result );
////	if (partial_result > 1800 and partial_result < 2200)
	    std::memcpy(&t1, &ttmp, sizeof(timeval));
////	else
////	    if (partial_result > 2000)
////		std::memcpy(&t1, &ttmp, sizeof(timeval));
//	gettimeofday(&t1, NULL);
//	printf(" %lu ", ((t2.tv_sec - t1.tv_sec) * 1000 + (t2.tv_usec - t1.tv_usec)/1000 ) );
//	t1=t2;
//	fflush (stdout);
//	sleep(1); 
}

int main(int argc, char *argv[])
{
    FILE *inp = fopen("bufferin", "rb");
    FILE *inp2 = fopen("bufferout", "wb");
    short int_to_char;
    gettimeofday(&t1, NULL);

    // current code word
    unsigned long int code_word = 0;

    wiringPiSetup () ;
    pullUpDnControl(7, PUD_DOWN);
    pinMode (0, OUTPUT) ;
    delay(100);
    //piThreadCreate (lowPwm); // enable on the other side <<<<<<<<<<<<<------------
    wiringPiISR (7, INT_EDGE_RISING, &syncClock) ;

    // iterate through byte stream
    while(true) {
        // read next byte
        int b = fgetc(inp);
        fputc(b, inp2);

        // quit at end of byte stream
        if (b == EOF) {
            break;
        }

        // track 32-bit code word
        code_word = (code_word | b) << 8;

        // NALU start
        if (code_word == 0x00000100) {
		// got nal flag, lets add timestamp
		gettimeofday(&t2, NULL);
		int_to_char= (t2.tv_sec - t1.tv_sec) * 1000 + (t2.tv_usec - t1.tv_usec)/1000;

		fputc( (int_to_char & 0xff ) ,inp2);
		fputc( ((int_to_char >> 8) & 0xff ) ,inp2);
		printf(" %lu ", int_to_char );
                // read nalu type
                int type = fgetc(inp);
		fputc(type, inp2);

                // quit at end of byte stream
                if (type == EOF) {
                    break;
                }

                // ignore reserved bit
                type >>= 1;

                if (type == 19 /* IDR */) {
                    printf("NALU type: IDR\n");
                }
                else {
                    printf("NALU type: %d\n", type);
                }
                code_word = 0;
        }
    }

    fclose(inp);
    fclose(inp2);
    return 0;
}
