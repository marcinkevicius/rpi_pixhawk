#include <cstdlib>
#include <cstdio>
#include <sys/time.h>
#include <wiringPi.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>
#include <time.h>

static struct timeval t1, t2;
// the file descriptor representing the connection to the client
int connfd = -1;
bool main_finished, child_finished;
int filling_index=0; // this array index indicates current data buffer being filled
// the buffer to store text
char frames_data[10][50010];
int frames_count[10];
using namespace std;

//PI_THREAD (lowPwm) {
//	for (;;)
//  	{
//    		digitalWrite (0, HIGH) ; delay (200) ;
//    		digitalWrite (0,  LOW) ; delay (1800) ;
//  	}
//}

void syncClock (void) {
	gettimeofday(&t1, NULL);
}

PI_THREAD (send_frame) {
    struct timespec timeOut,remains;
    timeOut.tv_sec = 0;
    timeOut.tv_nsec = 5000000; /* 5 milliseconds */

    child_finished=false;

    int x;

    while(!main_finished){
        x = (filling_index+3) % 10 ; // move 3 positions to from and start searching for filled up buffers

        while( x != filling_index ) { // lets iterata throught array and search for data to be send

            if (frames_count[x]) {  // checking if there is a valid data
                if(write(connfd, frames_data[x], frames_count[x]) < 0) {
                    printf("write");
                    child_finished = true;
                } else {
                    frames_count[x] = 0;
//                    printf("sb %d", x);
                }
            }
            x = (x+1)%10;
        }
        nanosleep(&timeOut, &remains); //we have sent all the data, so lets sleep a little
    }
    child_finished = true; // done our job, lets quit
}

int main(int argc, char *argv[])
{

    struct timespec timeOutC,remainsC;
    timeOutC.tv_sec = 0;
    timeOutC.tv_nsec = 10000000; /* 10 milliseconds */

    main_finished = false;
    child_finished = true;
    if (argc != 3)
        return 1;

    // the structures representing the server address
    sockaddr_in serverAddr;

    // get the port number
    int port = atoi(argv[2]);

    // make sure that the port is within a valid range
    if(port < 0 || port > 65535)
    {
	    printf("Invalid port numbern");
	    return 1;
    }

    // stores the size of the client's address
    socklen_t servLen = sizeof(serverAddr);

    // connect to the server
    if((connfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
	    printf("socket");
	    return 1;
    }

    // set the structure to all zeros
    memset(&serverAddr, 0, sizeof(serverAddr));

    // set the server family
    serverAddr.sin_family = AF_INET;

    // convert the port number to network representation
    serverAddr.sin_port = htons(port);

    //  convert the IP from the presentation format (i.e. string)
    //  to the format in the serverAddr structure.
    if(!inet_pton(AF_INET, argv[1], &serverAddr.sin_addr))
    {
	    printf("inet_pton");
	    return 1;
    }

    // connect to the server. This call will return a socket used
    // used for communications between the server and the client.
    if(connect(connfd, (sockaddr*)&serverAddr, sizeof(sockaddr)) < 0)
    {
	    printf("connect");
	    return 1;
    }

    // bytes read from file
    unsigned long int bytes_read = 0;

    // bytes to read from (file or pipe)
    FILE *inp = fopen("bufferin", "rb");

    // time difference storring in char (8 bits = short)
    short int_to_char;
    gettimeofday(&t1, NULL);

    // current code word
    unsigned long int code_word = 0;
    filling_index = 0;
    frames_count[filling_index] = 0;

    wiringPiSetup () ;
    pullUpDnControl(7, PUD_DOWN);
    pinMode (0, OUTPUT) ;
    delay(100);
//    piThreadCreate (lowPwm); // enable this on the server side <<<<<<<<<<<<<------------
    wiringPiISR (7, INT_EDGE_RISING, &syncClock) ;
    piThreadCreate (send_frame);
    nanosleep(&timeOutC, &remainsC); // wait child to start the process

    // iterate through byte stream
    while(true) {
        // read next byte
        int b = fgetc(inp);
        //used to write to file with  fputc(b, inp2); function
        frames_data[filling_index][bytes_read] = b;
        bytes_read++;

        // quit at end of byte stream
        if (b == EOF) {
            frames_count[filling_index]=bytes_read; // set frame index with already bytes read
            filling_index = (filling_index +1)%10; // move to next index, and hope child thread will process it
            break; // get out of loop
        }

        if (bytes_read >= 50008){ // we are at the end of filling buffer, lets make sure we'll not cut NAL unit in half
            int new_filling_index = (filling_index+1)%10;
            int new_bytes_read = 0;

            for (int x=(bytes_read-4); x < bytes_read; x++) {
                frames_data[new_filling_index][new_bytes_read]=frames_data[filling_index][x];
                new_bytes_read++;
            }
            frames_count[filling_index]=bytes_read-new_bytes_read; // removing nalu from last buffer
            filling_index = new_filling_index; // setting new filling index
            bytes_read = new_bytes_read;
        }

        // track 32-bit code word
        code_word = (code_word | b) << 8;

        // NALU start
        if (code_word == 0x00000100) {
            if (child_finished) {
                printf("child ended with error");
                break;
            }
            // got nal flag, lets get timestamp
            gettimeofday(&t2, NULL);
            int_to_char= (t2.tv_sec - t1.tv_sec) * 1000 + (t2.tv_usec - t1.tv_usec)/1000;
            // got nalu flag, lets finnish buffer
            if (bytes_read == 4){ // if new buffer has 8 bytes - it means the nalu unit is at the beginning, do nothing.

            } else {
                // move nalu data to new buffer
                int new_filling_index = (filling_index+1)%10;
                int new_bytes_read = 0;

                for (int x=(bytes_read-4); x < bytes_read; x++) {
                    frames_data[new_filling_index][new_bytes_read]=frames_data[filling_index][x];
                    new_bytes_read++;
                }
                frames_count[filling_index]=bytes_read-new_bytes_read; // removing nalu from last buffer
                filling_index = new_filling_index; // setting new filling index
                bytes_read = new_bytes_read;
            }


            frames_data[filling_index][bytes_read] = (int_to_char & 0xff) ;
            bytes_read++;
            frames_data[filling_index][bytes_read] = ((int_to_char >> 8) & 0xff );
            bytes_read++;

            // prints char as tyme diff
            //printf(" %lu ", int_to_char );

            // read nalu type
            int type = fgetc(inp);
            frames_data[filling_index][bytes_read] = type;
            bytes_read++;

            // quit at end of byte stream
            if (type == EOF) {

                frames_count[filling_index]=bytes_read; // set frame index with already bytes read
                filling_index = (filling_index +1)%10; // move to next index, and hope child thread will process it
                break; // get out of loop

            }

            // ignore reserved bit
            type >>= 1;


            // jus for debugging
//            if (type == 19 /* IDR */) {
//                printf("NALU type: IDR\n");
//            }
//            else {
//                printf("NALU type: %d\n", type);
//            }

            code_word = 0;
        }
    }

    main_finished=true;

    nanosleep(&timeOutC, &remainsC); // wait child to finis the process

    while(!child_finished){
        nanosleep(&timeOutC, &remainsC); // wait child to finis the process
    }

    close(connfd);
    fclose(inp);

    return 0;
}
// compile :
// gcc read_nal_server_side_tcp_stream.cpp  -L/usr/local/lib -lwiringPi -lwiringPiDev -lpthread -lm
// stream :
// (./a.out 192.168.100.251 5001) & raspivid -t 50000 -w 1296 -h 972 -fps 49 -b 5000000 -o bufferin